<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55) ), url("/img/music/xhunters.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 40%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>X-HUNTERS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/C4L2b4r2X8M"></iframe>
                                </div>
                        </div>
                        <div class='music-social-media'>
                            <center>                      
                                <a href='https://xhunters.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://facebook.com/TheXHunters/'><img src="/img/facebookband.png"></a>
                                <a href='http://youtube.com/user/xhuntersband'><img src="/img/YouTubeBand.png"></a>        
                            </center>
                        </div>
                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>The X-Hunters are an instrumental metal band from New Jersey paying tribute to the SNES Mega Man X Series. 
                            Known for a thunderous assualt of heavy drums and soaring twin leads, the X-Hunters should please fans of metal 
                            and video game music alike.</p>
                            <br><center><img src="/img/bigadam.jpg"></center></br> 
                            <h2>Also, at least 75% of the X-Hunters are Adam Chase's dad and he's very proud of them.</h2>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
