<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/baroqueen.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 30%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>WISHLYST</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/f6gKFazaGjU"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://gamechops.com'><img src="/img/Website.png"></a>
                                <a href='http://soundcloud.com/wishlyst'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/wishlyst'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/wishlystmusic/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/channel/UCoXu6d_3_xZJERz2XWnz8gw'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Featuring her vocals, songwriting, and electronic production, Wishlyst brings her extensive musical 
                            training to a new medium. Taking influences from electronic producers, pop culture, fashion, and video 
                            games, Wishlyst lets us into her emotional world.</p>
                            <p>As a DJ, Wishlyst fuses pop, hip hop, future bass, and videogame-inspired EDM to make cohesive and 
                            detailed mixes with undeniable girl-power vibes.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
