<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/pixel8ter.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 0% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>PIXEL8TER</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZnCJ97fLRd0"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://grimecraft.com'><img src="/img/Website.png"></a>
                                <a href='http://pixel8ter.bandcamp.com'><img src="/img/BandCamp.png"></a>
                                <a href='http://soundcloud.com/pixel8ter'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/Pixel8ter/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/pixel8ter/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/user/GameChops'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Groovey video game inspired jams meets with live vocoder vocals and interactive controllers, Pixel8ter isn't just a DJ, he's a one man machine band</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
