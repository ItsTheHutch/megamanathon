<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/shubzilla.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 25%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>SHUBZILLA</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uz2Aw7kkkWk"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://npccollective.com'><img src="/img/Website.png"></a>
                                <a href='http://shubzilla.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/shubzilla'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/shubzilla/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/shubzilla/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/channel/UCudjfXlXNoAUm4MIreYmDuQ'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Shubzilla is a rapper based out of Renton, WA. No stranger to the stage, Shubzilla turned to rap as a creative 
                                after years of training as a dancer. She has never looked back, addicted to fierce vocals that charge the 
                                audience. Remaining true to her former performance style, Shubzilla’s sets are high energy. Her lyrics inviting 
                                the crowd to participate and become part of the show.</p>
                            <p>Already a staple in nerdcore rap, Shubzilla aims to be a part of the growing Seattle music community and beyond. 
                                She has worked and collabed with Bill Beats, Lex Lingo (as 9k1), Death*Star, Klopfenpop, as well as her family 
                                in rhyme The NPC Collective. Shubzilla has already performed in venues such as the Nectar Lounge, Substation, 
                                Lo-Fi, and the Funhouse. She has also performed along the west coast for the Day Jobs Tour: West Coast Edition 
                                and for other events such as Orlando Nerd Fest and Nerdapalooza.</p>
                            <p>Shubzilla is a co-founder of The Nerdy People of Color Collective, a group of nerds of color who strive to promote 
                                diversity and inclusion in geek culture and nerd spaces. She is also a co-founder and a staff member for GeekGirlCon, 
                                a Seattle-based organization dedicated to celebrating the female geek.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
