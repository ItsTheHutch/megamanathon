<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15) ), url("/img/music/amandalepre.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 50%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>AMANDA LEPRE</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/47VK3I4Zo_4"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://www.amandalepre.com/'><img src="/img/Website.png"></a>
                                <a href='https://amandalepre.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='https://soundcloud.com/amandalepre'><img src="/img/SoundCloud.png"></a>
                                <a href='https://www.facebook.com/amandalepre'><img src="/img/facebookband.png"></a>
                                <a href='https://twitter.com/amandalepre'><img src="/img/TwitterBand.png"></a>        
                                <a href='https://www.youtube.com/user/amandalepre'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>With rich soprano vocals and aggressive, metal-inspired acoustic riffs, Amanda Lepre's music is like 
                            strapping on armor and suiting up on an adventure. As a vocalist, guitarist, arranger, and performer, she 
                            blends influences from video game music and literature to create her own singer/songwriter style.</p>
                            <p>In addition to her original music, Amanda is the founder and front-woman of Austin-based video game 
                            tribute band Descendants of Erdrick, and was recently selected by rock icon Andrew W.K. to perform in 
                            his touring band. Her debut album Beneath the Forest of Error contains 11 tracks of heavy, progressive/electronic-inspired 
                            melodies in a foundation of acoustic riffs.</p>
                            <p>Amanda Lepre is originally from the Rio Grande Valley, and is currently based in Austin, Texas.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
