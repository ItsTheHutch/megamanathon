<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15) ), url("/img/music/crunkwitch.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 30%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>CRUNK WITCH</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/p2fF24ftWPg"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://crunkwitch.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='https://soundcloud.com/crunk-witch'><img src="/img/SoundCloud.png"></a>
                                <a href='https://www.facebook.com/crunkwitchmusic/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/crunkwitch/'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/channel/UCWziw0BMBHTSIdHO7Xh-ODA'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Crunk Witch is an electro-pop duo from Presque Isle, ME. The band was formed in 2009 by married couple Brandon Miles and Hannah Colleen.</p>
                            <p>Their music is a blend of pop, rock, chiptune, and various electronic music genres. Many of their lyrics are set in a science fiction 
                                reality and involve themes of adventure and romance.</p>                            
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
