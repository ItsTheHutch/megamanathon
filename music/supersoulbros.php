<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15) ), url("/img/music/supersoulbros.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 50%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>SUPER SOUL BROS.</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/T5_asJZMhOE"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://supersoulbros.com/'><img src="/img/Website.png"></a>
                                <a href='http://supersoulbros.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='https://soundcloud.com/supersoulbros'><img src="/img/SoundCloud.png"></a>
                                <a href='http://www.facebook.com/supersoulbros'><img src="/img/facebookband.png"></a>
                                <a href='http://www.twitter.com/supersoulbros'><img src="/img/TwitterBand.png"></a>        
                                <a href='http://youtube.com/SuperSoulBros'><img src="/img/YouTubeBand.png"></a>    
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>Hailing from Silicon Valley, the Super Soul Bros bring an explosive performance of live, funky video game music. 
                            Renowned for their dynamic musicianship, improvisation and infectious grooves, the Super Soul Bros are a hit with all ages. 
                            Whether they're taking requests on the fly, or the crowd is busting into a full-on soul train, it's always a party with the 
                            Super Soul Bros.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
