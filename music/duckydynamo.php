<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15) ), url("/img/music/duckydynamo.jpg"); 
                            background-repeat: no-repeat;                            
                            background-position: 100% 10%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>Ducky Dynamo</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/356338472&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://www.soundcloud.com/duckydynamo'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/duckydynamo/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/duckydynamo/'><img src="/img/TwitterBand.png"></a>                                            
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>For a small period of life post Y2K, a Friday night in Baltimore, MD would involve maybe going to the mall for a Cinnabon or a new 
                            outfit. But all roads led to the Paradox where the legendary K-Swift's residency took place. From midnight to 6am; she was responsible 
                            for introducing the hottest tracks to a collection of the cities most dedicated nightlife constituents.
                            </p>
                            <p>Ducky Dynamo was one of those patrons. Eventually her love of music and the culture of her city would lead her to join various street 
                            teams and promotional teams all the while throwing her own events and always paying attention to the rotating cast of DJs associated with K-Swift 
                            including Supa DJ Big L, Mike Mumblez, Rod Lee & Blaqstarr to name a few. K-Swift passed leaving a huge void which has remained empty to this day 
                            some say.</p>
                            <p>With their leader gone; many club scene constituents disbanded and went on with their lives. After completing college at Towson University and 
                            spending a few years toiling among corporate America; fate would throw her back into the world of entertainment when in 2013 she began to manage 
                            upcoming American DJ, Shawn Smallwood. By 2016, she was integral in his ascension from underground DJ hero to a budding international star. During 
                            that time; she trained under him and began to play out as well, with the two forming the electronic production duo known as Turnt. Ducky Dynamo's 
                            antics have led her to play in some of Baltimore and DC's best known venues, as well as touring through the country and across our borders bringing 
                            the Baltimore Club and party sound to all within earshot.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
