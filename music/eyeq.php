<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35) ), url("/img/music/eyeq.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>EyeQ</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/154899816&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                                </div>
                        </div>

                        <div class='music-social-media'>
                            <center>                      
                                <a href='http://eyeq.bandcamp.com/'><img src="/img/BandCamp.png"></a>
                                <a href='http://www.soundcloud.com/musiceyeq'><img src="/img/SoundCloud.png"></a>
                                <a href='http://facebook.com/musiceyeq/'><img src="/img/facebookband.png"></a>
                                <a href='http://twitter.com/MusicEyeQ/'><img src="/img/TwitterBand.png"></a>          
                            </center>
                        </div>

                        <article class='col-xs-10 col-xs-offset-1'>   
                            <h2>Bio</h2>          
                            <p>NPCC Member EyeQ is an indie hip-hop artist, visionary, emcee whose musical sights are set on merging 
                                his wide assortment of musical influences and loves into one powerhouse perfomance. Rasied in Brooklyn, 
                                NY, now Florida based emcee. EyeQ has shared the stage with the likes of Doug Wimbish, legendary bassist 
                                for Living Colour, DiViNCi from Solillaquists of Sound, Mega Ran, MC Lars, Schaffer the Darkload, Sammus, 
                                Fresh Kils and founder of Grind time now Madd Illz, Eturn an SPS and many more. He is as busy in the studio 
                                as he is on the stage. He released his Attack on Titan themed album Titled "Walls of Maria" in 2015 an is 
                                currently working on his next album. In the meantime, he has been collaberating on tracks with artists like 
                                Richie Branson, Mega Ran, Kadesh Flow and Ben Briggs, just to name a few. EyeQ, formerly co-founder of the 
                                Nerdcore Super Hip-Hop group 'RPG-Unit,' who's debut album Rock Paper Genocide was featured on the front page 
                                of Kotaku, Bandcamp and also gained the acclaim of Randy Picthford and Gearbox.</p>
                        </article>

                    </div>                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
