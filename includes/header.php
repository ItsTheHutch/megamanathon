<header>          

    <a href="/"><img class="hidden-sm hidden-xs"src="/img/MegaManathonLogo.png"><img class="hidden-lg hidden-md"src="/img/MegaManathonLogoSmall.png"></a>

    <nav>
        <ul class="hidden-md hidden-sm hidden-xs">
        <!--
            <li><a href="http://megamanathon.com/donate" id="donate">DONATE</a></li>
            <li><a href="http://tracker.megamanathon.com/index/mmat7">TRACKER</a></li>
            <li><a href="http://horaro.org/megamanathon/mm7schedule">SCHEDULE</a></li>
            -->
            <!--<li><a href="/music.php">MUSIC</a></li>--> 
            <li><a href="/video.php">VIDEO</a></li>
            <li><a href="/about.php">ABOUT</a></li>
        </ul>

        <div class="side-nav hidden-lg">
            <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        </div>
    </nav>

    <script>
        function openNav() {
            document.getElementById("mySideNav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySideNav").style.width = "0";
        }
    </script>

</header>