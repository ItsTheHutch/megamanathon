<?php
    
    function convertIncentive($input){
        
        switch($input){
            case "mm2Difficult":
                return "Mega Man 2: Game will be played on Difficult";
                break;
                
            case "mm4Wily":
                return "Mega Man 4: Dr. Wily dies";
                break;
                
            case "mm5Balloon":
                return "Mega Man 5: Juggleboy will make balloon animals and hats";
                break;
                
            case "mmLegends":
                return "Mega Man Legends: Secret challenge for the couch commentator";
                break;
                
            case "mmx2Challenge":
                return "Mega Man X2: Challenge an audience member";
                break;
                
        }
        
    
    }

?>