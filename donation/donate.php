<?php
    session_start();
?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55) ), url("/img/donationheader.png"); 
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>DONATE TO CHILD'S PLAY!</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                <!-- BEGIN THE CODE TO HANDLE THE FORM SUBMISSION -->
                <?php 

                //define variables and set to empty values
                $amount = $name = $email = $message = $type = $incentive = "";
                $amountErr = $emailErr = "";
                $valid = 0;

                //Strip unnecessary characters (extra space, tab, newline) and remove backslashes (/)
                function testInput($data){
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                } 

                //If a POST, we're going to store the values to session variables
                if ($_SERVER["REQUEST_METHOD"] == "POST"){
                    $regex = "/^[0-9]+(\.[0-9]{1,2})?$/";
                    if(empty($_POST["amount"]) || !preg_match($regex, $_POST["amount"])){
                        $amountErr = "<p style='color:red;'>A VALID DONATION AMOUNT IS REQUIRED!</p>";
                    }
                    else if($_POST["amount"] < 1.00){
                        $amountErr = "<p style='color:red;'>MINIMUM DONATION AMOUNT IS $1!</p>";
                    }
                    else{
                        $amount = testInput($_POST["amount"]);
                        $_SESSION["amount"] = number_format($amount,2,'.','');
                        $valid = 1;
                    }
                    $_SESSION["name"] = testInput($_POST["name"]);
                    $_SESSION["email"] = testInput($_POST["email"]);
                    if(!empty($_SESSION["email"]) && !filter_var($_SESSION["email"], FILTER_VALIDATE_EMAIL)){
                        $emailErr = "<p style='color:red;'>INVALID EMAIL FORMAT!</p>";
                        $valid = 0;
                    }
                    $_SESSION["incentive"] = testInput($_POST["incentive"]);
                    $_SESSION["message"] = testInput($_POST["message"]);                 
                }

                if($valid == 1){               
                    if(!headers_sent()){
                        header("Location: ./confirm.php");
                    }
                    else{
                        ?>
                        <script type="text/javascript">
                        document.location.href="./confirm.php";
                        </script>
                        Thanks! One second, we're redirecting you...
                        <?php
                    }
                    exit();
                }
                else{ 
                    //else display the regular donation page and any validation errors
            ?>
            <!-- END THE CODE TO HANDLE THE FORM SUBMISSION -->
                    <div class="row">
                        <section class='col-xs-10 col-xs-offset-1'>   
                            <h2>A Few Things You Should Know...</h2>
                            <ul>
                                <li>          
                                <p>Your donation goes DIRECTLY to Child's Play. We don't touch it at all.</p>
                                </li>
                                <li>          
                                <p>We ask for the information below so we can contact you if you win a prize. If you want to donate 
                                anonymously, that's fine, we will only store the donation amount, but you will forfeit prize eligibility.</p>
                                </li>
                                <li>          
                                <p>Don't forget to choose an incentive to put your donation to! Check incentive progress below the form!</p>
                                </li>
                                <li><p>Alternatively, you can buy a Mega Man-athon shirt at <b><a href="http://theyetee.com">The Yetee</a></b> and $3 of 
                                each sale will go to Child's Play!</p></li>
                        </section>
                    </div>           

                    <div class="donation-form container-fluid">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 col-xs-offset-2">
                                                
                           
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" role="form">
                                <div class="form-group">
                               
                                    <label for="amount">Donation Amount (USD - $1 minimum)</label>
                                    <div class="input-group input-group-lg">
                                      <div class="input-group-addon">$</div>
                                        <input type="text" name="amount" placeholder="0.00 (REQUIRED)" class="form-control"> 
                                    </div>
                                    <?php echo $amountErr; //Display error if an amount was not provided. ?>
                                </div>
                                
                              
                               <div class="form-group">
                                   <label for="email">Email</label>
                                   <div class="input-group-lg">
                                   <input type="text" name="email" maxlength="60" placeholder="Enter your email" class="form-control"> <?php echo $emailErr; //Display error if an amount is forgotton ?>
                                   </div>
                               </div>
                               
                               <div class="form-group">
                    
                                   <label for="name">Name</label>
                                   <div class="input-group-lg">
                                   <input type="text" name="name" maxlength="60" placeholder="Enter your name (Anonymous if left blank)" class="form-control">
                                   </div>

                               </div>

                               <div class="form-group">
                    
                                    <label for="incentive">Choose an incentive:</label>
                                    <div class="input-group-lg">
                                    <select class="form-control" name="incentive">
                                        <option value="mm2Difficult">Mega Man 2: Game will be played on Difficult</option>
                                        <option value="mm4Wily">Mega Man 4: Dr. Wily dies</option>
                                        <option value="mm5Balloon">Mega Man 5: Juggleboy will make balloon animals and hats</option>
                                        <option value="mmLegends">Mega Man Legends: Secret challenge for the couch commentator</option>
                                        <option value="mmx2Challenge">Mega Man X2: Challenge an audience member</option>
                                    </select>
                                    </div>

                                </div>
                              
                               <div class="form-group">
                                   <label for="message">Message (3000 character limit)</label>
                                   
                                   <textarea name="message" rows="10" cols="30" maxlength="3000" class="form-control" placeholder="Any comments? We may share them on stream!"></textarea>
                                   
                               </div>


                              <center><button type="submit" class="btn btn-primary">Submit Donation</button></center>
                            </form>
                                                       
                            
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->    

                <br>
                <hr>
                <br>

                <?php 
                
                    include($_SERVER['DOCUMENT_ROOT'] . '/donation/incentives.php'); 
                    $grandTotal = getGrandTotal();
                ?>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">                        
                        <h2>Incentive Progress</h2>
                        
                            <center><h3>Help Us Beat Last Year's Total!</h3></center>
                            <center><span class="text-<?php echo returnColor($grandTotal,5200);?>"><h1 id="incentive-grand-total">$<?php echo $grandTotal;?></h1><h2 id="incentive-grand-total-goal"> / $5200</h2></span></center>                        
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor($grandTotal,10000);?>">
                            <div class="panel-heading"><b>Juggling Show Extends to Ten Minutes</b><br>Show after Mega Man 5 - All donations go to this incentive!</div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor($grandTotal,10000);?>">$<?php echo $grandTotal;?> / $5000</span></h2></center></div>
                        </div>
                    </div>
                </div>
                    
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor($grandTotal,5000);?>">
                            <div class="panel-heading"><b>Five Minute Juggling Show by Juggleboy</b><br>Show after Mega Man 5 - All donations go to this incentive!</div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor($grandTotal,5000);?>">$<?php echo $grandTotal;?> / $5000</h2></center></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor(getIncentiveStatus("mm2Difficult"),250);?>">
                            <div class="panel-heading"><b>Mega Man 2: Game will be played on Difficult</b></div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor(getIncentiveStatus("mm2Difficult"),250);?>">$<?php echo getIncentiveStatus("mm2Difficult");?> / $250</span></h2></center></div>                                    
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor(getIncentiveStatus("mm4Wily"),250);?>">
                            <div class="panel-heading"><b>Mega Man 4: Dr. Wily Dies in the Final Battle!</b></div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor(getIncentiveStatus("mm4Wily"),250);?>">$<?php echo getIncentiveStatus("mm4Wily");?> / $250</span></h2></center></div>                                    
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor(getIncentiveStatus("mm5Balloon"),250);?>">
                            <div class="panel-heading"><b>Mega Man 5: Balloon Hats and Animals Made by Juggleboy</b><br>$25 donations and up can request an animal!</div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor(getIncentiveStatus("mm5Balloon"),250);?>">$<?php echo getIncentiveStatus("mm5Balloon");?> / $250</span></h2></center></div>                                    
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor(getIncentiveStatus("mmLegends"),250);?>">
                            <div class="panel-heading"><b>Mega Man Legends: Secret Challenge for the Couch Commentator</b></div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor(getIncentiveStatus("mmLegends"),250);?>">$<?php echo getIncentiveStatus("mmLegends");?> / $250</span></h2></center></div>                                    
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-<?php echo returnColor(getIncentiveStatus("mmx2Challenge"),250);?>">
                            <div class="panel-heading"><b>Mega Man X2: Secret Challenge for an Audience Member</b></div>
                            <div class="panel-body"><center><h2><span class="text-<?php echo returnColor(getIncentiveStatus("mmx2Challenge"),250);?>">$<?php echo getIncentiveStatus("mmx2Challenge");?> / $250</span></h2></center></div>                                    
                        </div>
                    </div>
                </div>

                    

                    


                <?php 
                } //end the else statement
                ?>    

                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
