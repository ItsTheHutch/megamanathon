<?php

function getIncentiveStatus($incentive){
    require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');

    $currentTotal = 0.00;

    try {
        $conn = new PDO($servername, $username, $password);               
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT TRUNCATE(SUM(Amount),2) FROM Transactions WHERE Incentive = :incentive"); 
        $stmt->bindParam(':incentive', $incentive);
        $stmt->execute();
        $total = $stmt->fetch();
        $currentTotal = $total[0];

        if($currentTotal == NULL){
            $currentTotal = 0.00;
        }
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }           
    
    //End the database connection
    $conn = null;

    return $currentTotal;
}


function getGrandTotal(){
    require($_SERVER['DOCUMENT_ROOT'] . '/dbLogin.php');

    $currentTotal = 0.00;

    try {
        $conn = new PDO($servername, $username, $password);               
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT TRUNCATE(SUM(Amount),2) FROM Transactions WHERE Charity = 'childsplay'"); 
        $stmt->execute();
        $total = $stmt->fetch();
        $currentTotal = $total[0];

        if($currentTotal == NULL){
            $grandTotal = 0.00;
        }
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }           
    
    //End the database connection
    $conn = null;

    return $currentTotal;
}



function returnColor($amt,$goal){
    $divided = $amt/$goal;

    if($divided >= 0.5 && $amt < $goal){
        return "warning";
    }
    elseif($amt < $goal){
        return "danger";
    }
    elseif($amt >= $goal){
        return "success";
    }
}



?>