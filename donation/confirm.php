<?php
    /**
     *  Donation page (donate.php) will forward form inputs here so we can parse into variables, user  
     *  can confirm their input is correct, then we forward the values and user to PayPal.
     */

    session_start();
    require(__DIR__ . '/IpnListener.php');
    require(__DIR__ . '/convertIncentive.php');
    
    $amount = $_SESSION["amount"];
    $name = $_SESSION["name"];
    $email = $_SESSION["email"];
    $message = $_SESSION["message"];
    $incentive = $_SESSION["incentive"];
?> 

<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55) ), url("/img/donationheader.png"); 
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>DONATE TO CHILD'S PLAY!</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content container">

                    <div class="row">
                        <section class='col-xs-10 col-xs-offset-1'>   
                            <h2>Please check and confirm the below is correct...</h2>
                        </section>
                    </div>           

                    <div class="donation-form container-fluid">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 col-xs-offset-2">
                           
                            <?php
                                if ($name == ""){
                                    $name = "Anonymous";
                                }

                                echo "<div class='panel panel-primary'>
                                    <div class='panel-heading'>Donation amount (USD)</div>
                                    <div class='panel-body'>$".$amount."</div>
                                    </div>";

                                echo "<div class='panel panel-primary'>
                                    <div class='panel-heading'>Name</div>
                                    <div class='panel-body'>".$name."</div>
                                    </div>";

                                echo "<div class='panel panel-primary'>
                                    <div class='panel-heading'>Email</div>
                                    <div class='panel-body'>".$email."</div>
                                    </div>";

                                echo "<div class='panel panel-primary'>
                                    <div class='panel-heading'>Incentive</div>
                                    <div class='panel-body'>".convertIncentive($incentive)."</div>
                                    </div>";

                                echo "<div class='panel panel-primary'>
                                    <div class='panel-heading'>Message</div>
                                    <div class='panel-body'>".$message."</div>
                                    </div>";

                                //echo "<a href='donation.php'>Need to change something? Back to the Donation Form</a><br><br>";    
                            ?>

                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                            <input type="hidden" name="amount" value="<?php echo $amount;?>">
                            <input type="hidden" name="first_name" value="<?php echo $name;?>">
                            <input type="hidden" name="payer_email" value="<?php echo $email;?>">
                            <center><input type="submit" class="btn btn-success" name="submit" value="Continue to PayPal"></center>

                            <!-- Identify your business so that you can collect the payments. -->
                            <input type="hidden" name="business" value="donate@childsplaycharity.org">  <!--UNCOMMENT WHEN READY FOR PRODUCTION--> 
                            <!--<input type="hidden" name="business" value="chrikit-facilitator@gmail.com">-->
                            <!-- Specify a Donate button. -->
                            <input type="hidden" name="cmd" value="_donations">
                            <!-- Specify details about the contribution -->
                            <input type="hidden" name="item_name" value="Child's Play Charity donation via Mega Man-athon">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type="hidden" name="notify_url" value="http://megamanathon.tv/donation/ipn.php">
                            <input type="hidden" name="return" value="http://megamanathon.tv/donation/thankYou.php">
                            <!--<input type="hidden" name="notify_url" value="http://itsthehutch.com/donation/ipn.php">
                            <input type="hidden" name="return" value="http://itsthehutch.com/donation/thankYou.php">-->
                            <input type="hidden" name="custom" value="<?php echo $name;?>|<? echo $email;?>|<?php echo $message;?>|<?php echo $incentive;?>">

                            </form>
                            
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->          
                    


                </div><!--end main-content-->



            </div><!--end top-half-->            

            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>

    </body>
</html>
