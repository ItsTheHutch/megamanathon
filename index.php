<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="main-content-full">

                    <?php include(__DIR__ . '/includes/header.php'); ?>
   
                    <?php 
                        $test=1; //set to 1 if testing page before event, 0 for production
                        $date = time(); 
                        $eventStart = strtotime('2021-05-03 5:00:00');

                        function calculateTotalBar($amt,$goal){
                            $percentage = ($amt/$goal)*100;
                            if($percentage <= 5){
                                return "Orange005.png";
                            }
                            elseif($percentage > 5 && $percentage <= 10){
                                return "Orange010.png";
                            }
                            elseif($percentage > 10 && $percentage <= 15){
                                return "Orange015.png";
                            }
                            elseif($percentage > 15 && $percentage <= 20){
                                return "Orange020.png";
                            }
                            elseif($percentage > 20 && $percentage <= 25){
                                return "Orange025.png";
                            }
                            elseif($percentage > 25 && $percentage <= 30){
                                return "Orange030.png";
                            }
                            elseif($percentage > 30 && $percentage <= 35){
                                return "Orange035.png";
                            }
                            elseif($percentage > 35 && $percentage <= 40){
                                return "Orange040.png";
                            }
                            elseif($percentage > 40 && $percentage <= 45){
                                return "Orange045.png";
                            }
                            elseif($percentage > 45 && $percentage <= 50){
                                return "Orange050.png";
                            }
                            elseif($percentage > 50 && $percentage <= 55){
                                return "Orange055.png";
                            }
                            elseif($percentage > 55 && $percentage <= 60){
                                return "Orange060.png";
                            }
                            elseif($percentage > 60 && $percentage <= 65){
                                return "Orange065.png";
                            }
                            elseif($percentage > 65 && $percentage <= 70){
                                return "Orange070.png";
                            }
                            elseif($percentage > 70 && $percentage <= 75){
                                return "Orange075.png";
                            }
                            elseif($percentage > 75 && $percentage <= 80){
                                return "Orange080.png";
                            }
                            elseif($percentage > 80 && $percentage <= 85){
                                return "Orange085.png";
                            }
                            elseif($percentage > 85 && $percentage <= 90){
                                return "Orange090.png";
                            }
                            elseif($percentage > 90 && $percentage < 100){
                                return "Orange095.png";
                            }
                            elseif($percentage >= 100){
                                return "Orange100.png";
                            }
                        }

                        if($date > $eventStart || $test==0){

                    ?>

                    <div class="call-to-action container-fluid" id="jumbotron">

                    <?php $myfile = fopen("mm7total", "r") or die("Unable to open file!");
                    $donationTotal = fread($myfile,filesize("mm7total"));
                    fclose($myfile); ?>


                            <div class="row">
                                <div class="col-xs-12 col-md-10 col-md-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://player.twitch.tv/?channel=halfemptyetank"></iframe>
                                </div>
                                </div>
                            </div>
                            
                            <!-- ENABLE IF I GET THE SCRIPT STUFF WORKING
                            <div class="row">
                                <div class="col-xs-12 col-md-10 col-md-offset-1">
                                    <ul id="gameInfo">
                                        <li><img src="/img/controller50px.png"></li>
                                        <li id="gameName"><h3>Mega Man Legends</h3></li>  
                                    </ul>
                                    <ul id="playerInfo">
                                        <li><img src="/img/controller50px.png"></li>
                                        <li id="gameName"><h3>ItsTheHutch</h3></li>  
                                    </ul>
                                </div>
                            </div>
                            -->

                            <div class="row">
                                <div class="col-xs-12 col-md-10 col-md-offset-1">
                                    <h1 id="day-of-event">$<?php echo $donationTotal ?></h1>
                                    <?php  
                                        $image = calculateTotalBar($donationTotal,20000);
                                        $imagePath = "'/img/Orange/".$image."'";
                                    ?>
                                    <center><img id="progressBar" src=<?php echo $imagePath;?></center>
                                    <h2 id="underBar">Help us reach our goal of raising $20000 for Child's Play!</h2>
                                    <h2><b><a href="http://megamanathon.com/donate">Donate Now!</a><b></h2>
                                </div>
                            </div>

                            
                                
                    </div><!--end call-to-action-->

                    <?php 
                        }
                        else{ 
                    ?>


                    <div class="call-to-action container-fluid" id="jumbotron">

                                <!--<div class="row">
                                    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="http://player.twitch.tv/?channel=halfemptyetank"></iframe>
                                    </div>
                                    </div>
                                </div>-->                                
                                
                                <!--
                                <div class="row">
                                    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VHg7xJxFgGo"></iframe>
                                    </div>
                                    </div>
                                </div> 
                                -->
                                <!--            
                                <h1>$7,072 raised for Child's Play!</h1>
                                <h2>With 35 Mega Man games beaten, over 3 million viewers on Twitch and 1 Guinness World Record beaten for most wizards in one place, Mega Man-athon 6 was an event to remember!</h2>
                                <h2><b>Thank you to our viewers and donators!</b></h2>
                                <br><br>
                                <h3>Interested in participating next year? <a href="https://tinyurl.com/MMathon7signups">Sign up today!</a></h3>
                                <h5>Photo credit to Nick Mauer of MAGFest Mediatron</h5> -->

                                <h1>MEGA MANATHON 8</h1>
                                <h2><b>Returning April 2021!<b></h2>        
                                <h3>We're skipping 2020 due to the current challenges we're all facing right now. We will return next year with a new, week-long event with more Mega Man, 
                                    music and other shenanigans!  We will be supporting Direct Relief, who do great work for those in need all around the world. Check them out below! 
                                </h3>                     
                                <h3> Be sure to follow us on <a href="http://facebook.com/halfemptyenergytank">Facebook</a>, <a href="http://twitter.com/halfemptyetank">Twitter</a>, 
                                <a href="http://youtube.com/halfemptyenergytank">Youtube</a> and <a href="http://twitch.tv/halfemptyetank">Twitch</a> to keep up with the latest updates on Mega Manathon 8!</h3>
                                
                    </div><!--end call-to-action-->

                    <?php 
                        }
                    ?>

                    
                
                </div><!--end main-content-full-->

                <?php
                /*
                
                <div class="charity-info container-fluid">
                    <div class="row media">                            
                                <!--<div class="media-left media-middle">
                                    <a href="#">
                                    <img class="media-object hidden-xs hidden-sm" src="/img/childsplay_logo.jpg" alt="Child's Play Charity">
                                    </a>
                                </div> Is not showing up for some reason???? -->
                                <div class="media-body media-middle">
                                    <center><img class="" src="/img/childsplay_logo.jpg" alt="Child's Play Charity"></center>
                                    <h2 class="media-heading"><b><a href="/donation/donate.php">Donate Now to Support Child's Play!</a></b></h2>
                                    <p>Child's Play seeks to improve the lives of children in hospitals and domestic voilence shelters through the generosity and kindness of the video game industry and the power of play.</p>
                                    <p><a href="http://childsplaycharity.org/">Visit ChildsPlayCharity.org to learn more!</a></p>
                                </div><!-- end media-body div -->
                    </div><!-- end row -->
                </div><!-- end container-fluid -->
                
                */
                ?>


                <div class="charity-info container-fluid">
                    <div class="row media">                            
                                <!--<div class="media-left media-middle">
                                    <a href="#">
                                    <img class="media-object hidden-xs hidden-sm" src="/img/childsplay_logo.jpg" alt="Child's Play Charity">
                                    </a>
                                </div> Is not showing up for some reason???? -->
                                <div class="media-body media-middle">
                                    <center><img class="" src="/img/DirectRelief/drLogo.png" alt="Direct Relief"></center>
                                    <br>
                                    <div id="charity-image">                      
                                    </div>
                                    <br>
                                   <!-- <center><img class="" src="/img/DirectRelief/Photos/16.jpg" alt="Direct Relief"></center>-->
                                    <!--<h2 class="media-heading"><b><a href="/donation/donate.php">Donate Now to Support Direct Relief!</a></b></h2>-->
                                    <p>Direct Relief is a humanitarian aid organization, active in all 50 states and 70 countries, with a mission to improve the health and lives of people affected by poverty or emergencies.</p>
                                    <p>Direct Relief’s assistance programs focus on maternal and child health, the prevention and treatment of disease, and emergency preparedness and response, and are tailored to the particular circumstances and needs of the world’s most vulnerable and at-risk populations.</p>
                                    <br>
                                    <p id="charity-link"><a href="http://DirectRelief.org/">Visit DirectRelief.org to learn more!</a></p>
                                </div><!-- end media-body div -->
                    </div><!-- end row -->
                </div><!-- end container-fluid -->


                <!--
                <div class="prizes container-fluid">
                <h2>Donate to the kids and you may win a prize!</h2>
                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'>            
                                <h3>Minimum $1 Donation</h3>   
                            </article>
                        </div>
                    </div>
                    <center>
                            <ul class="prize-thumbs">       
                            <li><img src="img/prizes/KeyChainShadow.png" alt="8-bit Keychain"><p>8-bit Keychain</p></li>                        
                            </ul>
                    </center>
                        

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $5 Donation</h3>
                            </article>
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/WilyPoster.png" alt="Wily Ambitions Poster"><p>Wily Ambitions Poster</p></li>
                            <li><img src="img/prizes/RunGunPoster.png" alt="Run 'N Gun Poster"><p>Run 'N Gun Poster</p></li>
                            <li><img src="img/prizes/preview.png" alt="30th Anniversary Poster"><p>30th Anniversary Poster</p></li>
                            <li><img src="img/prizes/ComicVol1.jpg" alt="Mega Man Graphic Novel Vol. 1"><p>Mega Man Graphic Novel Vol. 1</p></li>
                            <li><img src="img/prizes/ComicVol2.jpg" alt="Mega Man Graphic Novel Vol. 2"><p>Mega Man Graphic Novel Vol. 2</p></li>
                        </ul>
                    </center>
                            

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $10 Donation</h3>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/emug_main_1024.png" alt="E-Mug"><p>E-Mug</p></li>
                        </ul>
                    </center>



                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $15 Donation</h3>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/Box.png" alt="Mega Man Legacy Collection"><p>Mega Man Legacy Collection w/ Gold Mega Man Amiibo</p><p>for Nintendo 3DS</p></li>
                        </ul>
                    </center>


                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h3>Minimum $25 Donation</h3>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="prize-thumbs">
                            <li><img src="img/prizes/Helmet.gif" alt="Mega Man helmet"><p>Wearable Mega Man Helmet</p></li>
                        </ul>
                    </center>                     

                </div>-->


                <?php
                /*
                <div class="sponsors container-fluid">
                    <h2>Thanks to Our Partners!</h2>
                    <br>
                    <ul>

                                
                                <li>
                                    <a href="http://magfest.org">
                                    <img class="" src="/img/magfest-partners.png" alt="MAGFest" height: "180px">
                                    </a>
                                </li>

                                <li>
                                    <a href="http://halfemptyetank.com">
                                    <img class="" src="/img/HEET.png" alt="Half Empty Energy Tank">
                                    </a>
                                </li>

                                <li>
                                    <a href="https://www.fangamer.com/">
                                    <img class="" src="/img/fangamer-logo.png" alt="Fangamer">
                                    </a>
                                </li>

                               <!-- <li>
                                    <a href="http://theyetee.com">
                                    <img class="" src="/img/yetee-logo.png" alt="The Yetee">
                                    </a>
                                </li>-->
                                <li>
                                    <a href="http://capcom.com">
                                    <img class="" src="/img/capcom.png" alt="Capcom">
                                    </a>
                                </li>
                                
                                
                    </ul>
                </div><!-- end sponsors container-fluid -->
                */
                ?>



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>

    </body>
</html>
