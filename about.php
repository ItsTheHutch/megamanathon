<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("/img/aboutheader.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>ABOUT MEGA MAN-ATHON</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content">
                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <h1><b>In the year 20XX, a team of super-fighting robots join forces with one mission: to improve the lives of children and defeat 
                                    Dr. Wily once and for all...</b></h1>

                                <p>For 72 hours, this team will use their unique abilities, speed and tenacity to siege Wily’s Castle.<p>
                                
                                <p><b>This is the Mega Man-athon--an annual, non-stop gauntlet of Mega Man, Mega Man-inspired games and music to raise money for 
                                    Child’s Play charity--presented by MAGFest!</b></p>                
                                
                                <p>Each year, as the Mega Man-athon team returns to take on more of Wily’s Robot Masters, they are joined by new allies: guest 
                                    players, speedrunners, game developers, YouTubers and more! Each event offers nightly live musical performances, featuring 
                                    some of the most diverse, talented acts the VGM scene has to offer.</p>

                                <p>Even you can join the fight! Donate to the Mega Man-athon to help the team to victory. Your donation might even win you a prize!</p>

                                <p>100% of proceeds donated to the Mega Man-athon go to Child’s Play. Child's Play is a charity that raises money to provide games, 
                                    peripherals, consoles and more to children's hospitals, therapy centers and domestic violence shelters. You might even say 
                                    Child’s Play helps children GET EQUIPPED with valuable resources to provide them comfort, entertainment and healthy distraction 
                                    as they fight their own battles.</p>

                                <p>Child’s Play is a well-established charity organization in the video game community. An astounding 95% of all donations to Child’s Play 
                                    go directly to benefit the hospitals (only 5% administrative overhead). You can learn more about Child's Play at 
                                    <a href="http://childsplaycharity.org">childsplaycharity.org</a>.

                                <p>The Mega Man-athon is hosted by <a href="http://halfemptyetank.com">Half Empty Energy Tank</a> (HEET).  The event is held every 
                                year at the Gaylord National Resort in National Harbor, MD during Super MAGFest and is streamed live on Twitch and 
                                <a href="http://megamanathon.tv">megamanathon.tv</a> for the full 72 hours of the event.</p>
                                <br>
                                <hr>
                                <br>      


                                <center>
                                <img src="/img/MegaManathon01logo.png">
                                <h2>January 3-6 2013</h2>
                                </center>        
                                <p>In the year 2013, one man conceived a plan to fight Dr. Wily, but he could not do it alone! He gathered the members of Half Empty 
                                    Energy Tank together and put his plan into motion: the first ever Mega Man-athon.</p>
                                <p>Held at MAGFest 11 in the console room, the first Mega Man-athon began with the goal of playing as many Mega Man games as possible 
                                    and raising money for Child's Play Charity. Special guests included: Stuttering Craig of Screwattack, Mega Ran, The Megas and The 
                                    Protomen!</p>
                                <p>The first Mega Man-athon was sponsored by Capcom as a part of the Mega Man 25th Anniversary celebration.</p>
                                <p>This event also commemorated Half Empty Energy Tank’s first ever live-streamed event—now one of countless broadcasts and events.</p>
                                <br>
                                <center><h2>$410 raised for Child's Play!</h2></center> 
                            </article>
                        </div>
                    </div>


                    <div id="mm1" >     
                            <style>
                                #mm1{
                                    background: url("/img/megamanathonset.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>

                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>   
                                <center>
                                <img src="/img/MegaManathon02logo.png">
                                <h2>January 2-5 2014</h2>
                                </center>          
                                <p>A year later, HEET brought the Mega Man-athon back to MAGFest! After a year of regular live-streaming, the team was better 
                                    prepared but also ready to take risks. Notable guests that took the time to play games included Andy of the X-Hunters, 
                                    Noah McCarthy of Bit Brigade and Jirard "The Completionist" Khalil!</p>
                                <br>
                                <center><h2>$1,190 raised for Child's Play!</h2></center>
                            </article>

                        </div>
                    </div>

                    <div id="mm2" >     
                            <style>
                                #mm2{
                                    background: url("/img/megamanathon2set.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>

                    

                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>   
                                <img src="/img/MegaManathon03logo.png">
                                <h2>January 23-26 2015</h2>
                                </center>          
                                <p>As the Mega Man-athon returned for a third year,  the stage expanded and moved from the console room to a corner in 
                                    front of the Expo Hall—a spot with fantastic views of the harbor and the Capital Wheel! The team welcomed numerous 
                                    notable guests at the Mega Man-athon 3, including: Jirard and Greg from That One Video Gamer, the Super Guitar Bros., 
                                    and Hot Pepper Gaming!</p>
                                <p>This was also the first year that the Mega Man-athon embraced the "music" part of the "Music and Gaming Festival." 
                                    Teaming up with Insert Coin Theater, D&ampD Sluggers, Mega Flare, Super Guitar Bros., 2 Mello and Sammus joined the 
                                    fight with amazing live performances.</p>
                                <br>
                                <center><h2>$4,103 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm3" >     
                            <style>
                                #mm3{
                                    background:url("/img/megamanathon3set.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>                                


                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>   
                                <img src="/img/MegaManathon04logo.png">
                                <h2>February 18-21 2016</h2>
                                </center>          
                                <p>As another year passed, the Mega Man-athon grew even stronger! Along with a wide variety of Mega Man games representing 
                                    the whole franchise, as well as some fan-made games, the Mega Man-athon 4 spotlighted the new Mega Man board game, which 
                                    was a crowd favorite of the year. Live music returned to the Mega Man-athon stage in force, featuring 3 nightly music 
                                    blocks with performances from: Ben Briggs, Tetracase, Ralfington, Grimecraft, DjCUTMAN, 2 Mello, Shadow Clone, LucioPro, 
                                    D&ampD Sluggers, Mega Ran, Super Guitar Bros., Crunk Witch, The Blast Processors, Sammus and Professor Shyguy!</p>
                                
                                <p>The Mega Man-athon 4 was also the first event to feature non-Mega Man games: Shovel Knight and Azure Striker Gunvolt. Matt 
                                    Papa of Inti Creates, developers of the Mega Man Zero and Mega Man ZX series, and Gunvolt voice actress Megu Sakuragawa 
                                    took to the stage to showcase Azure Stiker Gunvolt.</p> 
                                
                                <p>The fourth annual Mega Man-athon raised a total of $5,200 for Child’s Play!</p>
                                <br>
                                <center><h2>$5,200 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm4" >     
                            <style>
                                #mm4{
                                    background:url("/img/grimecraft.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>




                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>   
                                <img src="/img/MegaManathon05logo.png">
                                <h2>January 5-8 2017</h2>
                                </center>          
                                <p>In 2017 Mega Man-athon celebrated it’s 5th anniversary and most successful year. We were officially adopted into the 
                                    MAGFest family, combining our efforts with the Charity Department. Along with our new family, we found a new 
                                    home -- we said goodbye to our hallway that overlooked the harbor and found ourselves in a much grander space, with a 
                                    much more ambitious stage.</p>
                                
                                <p>Mega Man shenanigans continued as usual, featuring many games from the franchise as well as fan-made games, a new Mega Man 
                                    Power Hour block and a Mega Man 8-bit Deathmatch tournament. Special guests included Jirard “The Completionist” Khalil, Game Dave, 
                                    Gaijin Goomba, and My Life in Gaming.</p> 
                                
                                <p>Live musical performances happened every night starting with a GameChops Rave Night. Musical guests included Chjolo, 
                                    Pixel8ter, Ralfington, GrimeCraft, Baroqueen, Ben Briggs, DjCUTMAN, and Mykah. </p>

                                <p>Friday featured The NPC Collective and included Mega Ran, Sammus, D&D Sluggers, Eye-Q, K-Murdock, 1-UP, Doug Funnie, 
                                    Kadesh Flow, and Shubzilla.</p>

                                <p>Last but not least, Saturday was our HEET Showcase and included the Triforce Quartet, Super Soul Brothers, Professor 
                                    Shyguy, Super Guitar Brothers and LucioPro.</p>
                                <br>
                                <center><h2>$10,050 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm5" >     
                            <style>
                                #mm5{
                                    background:url("/img/aboutPageMM5.png");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>




                    <div class="container">
                        <div class="row">
                            <article class='col-xs-10 col-xs-offset-1'>
                                <center>  <br><br> 
                                <img src="/img/MegaManathon06logo.png">
                                <br><br>
                                <h2>January 4-7 2018</h2>
                                </center>          
                                <p>In 2018 Mega Man-athon celebrated Mega Man's 30th anniversary and the announcement of a brand new Mega Man game,
                                Mega Man 11!  We tried to include as many different Mega Man series as we could, including speed runs of Mega Man 
                                Legends 1, 2 and The Misadventures of Tronne Bonne!</p>
                                
                                <p>We also put out the call for all wizards as we attempted to break the Guiness World Record for most wizards 
                                in one spot!  We a greater turnout than we could have imagined, with over 700 wizards showing up to party with us!</p> 
                                
                                <p>Live musical performances happened every night starting with the NPC Collective! Musical guests included MCOHM-I, 
                                Kadesh Flow, D&D Sluggers, Eye-Q, Amanda Lepre, Shubzilla, Bill Beats and Sammus!</p>

                                <p>Friday featured The Wizard Party and included 2 Mello, Steel Samurai, The Flabbercasters, The X-Hunters and Super 
                                Soul Bros.!</p>

                                <p>Finally Saturday was the GameChops Rave Night and included FlexStyle, VGR, Ralfington, Wishlyst, Jadeabella, James Landino, 
                                Ben Briggs, Grimecraft and DJ Cutman!</p>
                                <br>
                                <center><h2>$7,097 raised for Child's Play!</h2></center>  
                            </article>
                        </div>
                    </div>

                    <div id="mm6" >     
                            <style>
                                #mm6{
                                    background:url("/img/wizardparty.jpg");
                                    height: 70vh;
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover; 
                                }
                            </style>                 
                    </div>



                </div>



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
