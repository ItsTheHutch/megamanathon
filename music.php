<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45) ), url("/img/robotmasters.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 20%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>LIVE MUSIC</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content">                                            

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h2>Thursday - The NPC Collective LIVE</h2>
                            </article>
                        </div>
                    </div>
                    <center>
                        <ul class="music-thumbs">
                            <li><a href="/music/duckydynamo.php"><img src="img/music/thumbnails/Ducky.png" alt="Ducky Dynamo"></a></li>
                            <li><a href="/music/sammus.php"><img src="img/music/thumbnails/Sammus.png" alt="Sammus"></a></li>
                            <li><a href="/music/ddsluggers.php"><img src="img/music/thumbnails/DDsluggers.png" alt="D&D Sluggers"></a></li>
                            <li><a href="/music/eyeq.php"><img src="img/music/thumbnails/EyeQ.png" alt="EyeQ"></a></li>
                            <li><a href="/music/mcohmi.php"><img src="img/music/thumbnails/OhmI.png" alt="MC Ohm-I"></a></li>
                            <li><a href="/music/amandalepre.php"><img src="img/music/thumbnails/Lepre.png" alt="Amanda Lepre"></a></li>
                            <li><a href="/music/kadeshflow.php"><img src="img/music/thumbnails/Kflow.png" alt="Kadesh Flow"></a></li>                        
                            <li><a href="/music/shubzilla.php"><img src="img/music/thumbnails/Shubzilla.png" alt="Shubzilla"></a></li>
                        </ul>
                    </center>
                            

                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'> 
                                <h2>Friday - Wizard Party</h2>
                            </article>                                
                        </div>
                    </div>
                    <center>
                        <ul class="music-thumbs">
                            <li><a href="/music/crunkwitch.php"><img src="img/music/thumbnails/CrunkWitch.png" alt="Crunk Witch"></a></li>
                            <li><a href="/music/2mello.php"><img src="img/music/thumbnails/2Mello.png" alt="2 Mello"></a></li>
                            <li><a href="/music/flabbercasters.php"><img src="img/music/thumbnails/flabbercasters.jpg" alt="Flabbercasters"></a></li>
                            <li><a href="/music/xhunters.php"><img src="img/music/thumbnails/XHunters.png" alt="X-Hunters"></a></li>
                            <li><a href="/music/supersoulbros.php"><img src="img/music/thumbnails/SuperSoulBros.png" alt="Super Soul Bros."></a></li>
                        </ul>
                    </center>
                    
                    <div class="container">
                        <div class="row"> 
                            <article class='col-xs-10 col-xs-offset-1'>            
                                <h2>Saturday - GameChops Rave Night</h2>   
                            </article>
                        </div>
                    </div>
                    <center>
                            <ul class="music-thumbs">       
                            <li><a href="/music/flexstyle.php"><img src="img/music/thumbnails/Flexstyle.png" alt="Flexstyle"></a></li>
                            <li><a href="/music/jameslandino.php"><img src="img/music/thumbnails/Landino.png" alt="James Landino"></a></li>                                         
                            <li><a href="/music/ralfington.php"><img src="img/music/thumbnails/Ralfington.png" alt="Ralfington"></a></li>
                            <li><a href="/music/grimecraft.php"><img src="img/music/thumbnails/Grimecraft.png" alt="Grimecraft"></a></li>
                            <li><a href="/music/wishlyst.php"><img src="img/music/thumbnails/Wishlyst.png" alt="Wishlyst"></a></li>
                            <li><a href="/music/benbriggs.php"><img src="img/music/thumbnails/BenBriggs.png" alt="Ben Briggs"></a></li>                                                        
                            <li><a href="/music/djcutman.php"><img src="img/music/thumbnails/Cutman.png" alt="DjCUTMAN"></a></li>
                            <li><a href="/music/vgr.php"><img src="img/music/thumbnails/VGR.png" alt="VGR"></a></li>
                            <li><a href="/music/jadeabella.php"><img src="img/music/thumbnails/DJadeabella.png" alt="MC Ohm-I"></a></li>
                            </ul>
                    </center> 
                   

                </div>   



            </div><!--end top-half-->
            
            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
