<!doctype html>
<?php
$page = $_SERVER['PHP_SELF'];
$sec = "60";
?>
<html>
    <head>
        <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
            @font-face {
                    font-family:"Futura Bold";
                    src:url("/fonts/futura-condensed-bold.ttf") format("truetype");
                    font-weight:normal;
                    font-style:normal;
                }

            body{
                font-family: 'Futura Bold';
                font-size: 18pt;
                margin-top: -0.5px;
                padding-left: 2px;
                color: white;
                background-color: #00ff00;   
                text-shadow: -2px 3px #000;             
            }

            .alignment{
                /*position: absolute;
                left: 0px;*/
            }
        </style>
        <?php
            function calculateTotalBar($amt,$goal){
                $percentage = ($amt/$goal)*100;
                if($percentage <= 5){
                    return "Orange005.png";
                }
                elseif($percentage > 5 && $percentage <= 10){
                    return "Orange010.png";
                }
                elseif($percentage > 10 && $percentage <= 15){
                    return "Orange015.png";
                }
                elseif($percentage > 15 && $percentage <= 20){
                    return "Orange020.png";
                }
                elseif($percentage > 20 && $percentage <= 25){
                    return "Orange025.png";
                }
                elseif($percentage > 25 && $percentage <= 30){
                    return "Orange030.png";
                }
                elseif($percentage > 30 && $percentage <= 35){
                    return "Orange035.png";
                }
                elseif($percentage > 35 && $percentage <= 40){
                    return "Orange040.png";
                }
                elseif($percentage > 40 && $percentage <= 45){
                    return "Orange045.png";
                }
                elseif($percentage > 45 && $percentage <= 50){
                    return "Orange050.png";
                }
                elseif($percentage > 50 && $percentage <= 55){
                    return "Orange055.png";
                }
                elseif($percentage > 55 && $percentage <= 60){
                    return "Orange060.png";
                }
                elseif($percentage > 60 && $percentage <= 65){
                    return "Orange065.png";
                }
                elseif($percentage > 65 && $percentage <= 70){
                    return "Orange070.png";
                }
                elseif($percentage > 70 && $percentage <= 75){
                    return "Orange075.png";
                }
                elseif($percentage > 75 && $percentage <= 80){
                    return "Orange080.png";
                }
                elseif($percentage > 80 && $percentage <= 85){
                    return "Orange085.png";
                }
                elseif($percentage > 85 && $percentage <= 90){
                    return "Orange090.png";
                }
                elseif($percentage > 90 && $percentage < 100){
                    return "Orange095.png";
                }
                elseif($percentage >= 100){
                    return "Orange100.png";
                }
            }
                ?>

    </head>
    <body>
        <div class="alignment">
            <?php $myfile = fopen("mm6total", "r") or die("Unable to open file!");
                    $donationTotal = fread($myfile,filesize("mm6total"));
                    fclose($myfile); ?>

            <center><h1 id="day-of-event">Help us reach our goal of $20000!<br>$<?php echo $donationTotal; ?> / $20000</h1></center>
                            <?php  
                                $image = calculateTotalBar($donationTotal,20000);
                                $imagePath = "'/img/Orange/".$image."'";
                            ?>

            <center><img src=<?php echo $imagePath;?></center>
               
        </div>        
    </body>
</html>
