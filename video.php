<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include(__DIR__ . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include(__DIR__ . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include(__DIR__ . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45) ), url("/img/inti.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 30%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>VIDEO ON-DEMAND</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end parallax-->

                <div class="main-content video-page-selection">
                    <div class="container">                        
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <div class="video-page-selection">
                                    <br><br>
                                    <a href="/video/megamanathon6.php"><img src="/img/MegaManathon06logo.png"></a>
                                    <br><br><br>
                                    <!--<br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-1yIPlIADOE"></iframe>
                                    </div>
                                    <br>-->
                                    <p>
                                </div>
                                <hr>
                                <div class="video-page-selection">
                                    <a href="/video/megamanathon5.php"><img src="/img/MegaManathon05logo.png"></a>
                                    <!--<br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-1yIPlIADOE"></iframe>
                                    </div>
                                    <br>-->
                                    <p>
                                </div>
                                <hr>
                                <div class="video-page-selection">
                                    <a href="/video/megamanathon4.php"><img src="/img/MegaManathon04logo.png"></a>
                                    <!--<br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-1yIPlIADOE"></iframe>
                                    </div>
                                    <br>-->
                                    <p>
                                </div>
                                <hr>
                                <div class="video-page-selection">
                                    <a href="/video/megamanathon3.php"><img src="/img/MegaManathon03logo.png"></a>
                                   <!-- <br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2nbwjilLzlw"></iframe>
                                    </div>
                                    <br>-->     
                                </div>
                                <hr>
                                <div class="video-page-selection">
                                    <a href="/video/megamanathon2.php"><img src="/img/MegaManathon02logo.png"></a>
                                    <!--<br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/z5FFDq2cnwE"></iframe>
                                    </div>
                                    <br>-->
                                </div> 
                                <hr>
                                <div class="video-page-selection">
                                    <a href="/video/megamanathon1.php"><img src="/img/MegaManathon01logo.png"></a>                                     
                                </div> 
                            </div>
                        </div>
                    </div>
                </div> 




                        
                                      




            </div><!--end top-half-->           

            <?php include(__DIR__ . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->

        <?php include(__DIR__ . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
