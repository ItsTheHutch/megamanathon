/*Create the table for the database*/

CREATE TABLE Transactions (
ID INTEGER NOT NULL AUTO_INCREMENT, 
Name VARCHAR(255), 
Email VARCHAR(255), 
Amount DECIMAL(15,2),
CollectedBy VARCHAR(40),
Charity VARCHAR(40),
Incentive VARCHAR(40),
Event VARCHAR(40), 
Message VARCHAR(3000), 
TimeOfDonation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,  
PRIMARY KEY (ID)
);

CREATE TABLE Users (
ID INTEGER NOT NULL AUTO_INCREMENT, 
Username VARCHAR(40), 
Password VARCHAR(200),
Role VARCHAR(40),
PRIMARY KEY (ID)
);

/*CREATE TABLE Groups (
ID INTEGER NOT NULL AUTO_INCREMENT, 
Username VARCHAR(40), 
Groupname VARCHAR(40), 
PRIMARY KEY (ID)
);

Insert garbage data

INSERT INTO Transactions (Name, Email, Amount, CollectedBy, Charity, Incentive, Message) 
VALUES ('John Cena', 'john.cena@wwe.com', 5000.01, 'web', 'childsplay', 'mm2Difficult', 'I LOOOOOOOOVE THE MARATHON AND DON\'T FORGET HIS NAME IS JOHN CENAAAAAAAA!');

INSERT INTO MegaManathon (FullName, Email, Incentive, Message, Amount, DonationType, Phone) 
VALUES ('John Cena', 'john.cena@wwe.com', 'Juggle show', 'for the kids!', 9000.01, 'Online', '555-555-5555');

INSERT INTO MegaManathon (FullName, Email, Incentive, Message, Amount, DonationType) 
VALUES ('John Cena', 'john.cena@wwe.com', 'Juggle show', 'that hutch guy is super great!', 9000.01, 'Online');


select * from MegaManathon;*/




/*DROP TABLE MegaManathon;*/