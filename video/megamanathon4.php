<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url("/img/briggspepper.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA MAN-ATHON 4 VIDEOS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">
                    
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr class="">
                                    <td>Game/Performance</td>
                                    <td>Player</td>
                                    <td>Video Length</td>
                                    <td>Details</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=-1yIPlIADOE">Mega Man-athon 4 promo</a></td>
                                    <td></td>
                                    <td>3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=M4R1lYnPY_M">Mega Man 3</a></td>
                                    <td>PixelKaye</td>
                                    <td>50 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=y3dklUA-_Io">Mega Man Unlimited</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>55 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=QNJTyUPZgEA">Ben Briggs LIVE</a></td>
                                    <td>Ben Briggs</td>
                                    <td>7 hr 51 min</td>
                                    <td>GameChops Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=h4IGHysOSm4">Tetracase LIVE</a></td>
                                    <td>Tetracase</td>
                                    <td>49 min</td>
                                    <td>GameChops Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=bo0gtFfFZcI">Ralfington LIVE</a></td>
                                    <td>Ralfington</td>
                                    <td>51 min</td>
                                    <td>GameChops Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=mojSPSXBkIg">Grimecraft LIVE</a></td>
                                    <td>Grimecraft</td>
                                    <td>1 hr</td>
                                    <td>GameChops Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=DFuWCMZzZOQ">DjCUTMAN LIVE</a></td>
                                    <td>DjCUTMAN</td>
                                    <td>1 hr 2 min</td>
                                    <td>GameChops Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=WSCTiJvK9n0">Mega Man 9</a></td>
                                    <td>Epic James</td>
                                    <td>1 hr 3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=VIYi2d_b3Q0">Mega Man 4 Minus Infinity</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>2 hr 10 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=S_d32burwfY">Mega Man: Dr. Wily's Revenge</a></td>
                                    <td>PixelKaye</td>
                                    <td>49 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=ZKTL-suvySQ">Rockman World 2</a></td>
                                    <td>PixelKaye</td>
                                    <td>42 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=GDh1CH7TmjM">Mega Man 3 (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>43 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51554152">Mega Man Soccer</a></td>
                                    <td>TheSkipper1995 &amp ChubRockGeek</td>
                                    <td> min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51559209">Mega Man &amp Bass</a></td>
                                    <td>ChubRockGeek</td>
                                    <td>1 hr 54 min</td>
                                    <td>Blind + unfinished playthrough</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51562427">Mega Man Rock Force</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 26 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51564755">Shovel Knight</a></td>
                                    <td>ChubRockGeek</td>
                                    <td>2 hr 34 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51574225">Mega Man Zero</a></td>
                                    <td>Jehm Faulking</td>
                                    <td>1 hr 15 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51576116">Mega Man ZX</a></td>
                                    <td>HotSammySliz</td>
                                    <td>1 hr 56 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51577910">Azure Striker Gunvolt</a></td>
                                    <td>Matt Papa</td>
                                    <td>1 hr 47 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51580284">LucioPro LIVE</a></td>
                                    <td>LucioPro</td>
                                    <td>47 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51582645">2 Mello &amp Shadow Clone LIVE</a></td>
                                    <td>2 Mello &amp Shadow Clone</td>
                                    <td>46 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51584635">D&ampD Sluggers LIVE</a></td>
                                    <td>D&ampD Sluggers</td>
                                    <td>54 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51585866">Mega Ran LIVE</a></td>
                                    <td>Mega Ran</td>
                                    <td>35 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51586813">Mega Man</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>54 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51588491">Mega Man X2</a></td>
                                    <td>My Life in Gaming</td>
                                    <td>1 hr 47 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51589279">Mega Man board game</a></td>
                                    <td>HEET</td>
                                    <td>1 hr 39 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51591029">Mega Man 5 (GameBoy)</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 19 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51594853">Mega Man Ultra</a></td>
                                    <td>HCHecxz</td>
                                    <td>1 hr 23 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51596813">Mega Man: Maverick Hunter X</a></td>
                                    <td>Kazooaloo</td>
                                    <td>2 hr 47 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51598258">Mega Man Zero 3</a></td>
                                    <td>Tterraj42</td>
                                    <td>2 hr</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51599750">Mega Man Battle Network</a></td>
                                    <td>Tterraj42</td>
                                    <td>1 hr 27 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51600538">Mega Man X</a></td>
                                    <td>Phonetic Hero</td>
                                    <td>50 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51601880">Mega Man Xtreme 2</a></td>
                                    <td>MegaFlare</td>
                                    <td>33 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51604286">Mega Man 8</a></td>
                                    <td>Ben Briggs</td>
                                    <td>2 hr 35 min</td>
                                    <td>Terrible Life Choices run</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51605299">Super Guitar Bros. LIVE</a></td>
                                    <td>Super Guitar Bros.</td>
                                    <td>45 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51606378">Crunk Witch LIVE</a></td>
                                    <td>Crunk Witch</td>
                                    <td>37 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51607870">Mega Man X4 race</a></td>
                                    <td>Jehm Faulking vs. Phoentic Hero</td>
                                    <td>49 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51609926">Professor Shyguy LIVE</a></td>
                                    <td>Professor Shyguy</td>
                                    <td>48 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51614599">Sammus LIVE</a></td>
                                    <td>Sammus</td>
                                    <td>46 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51616414">The Blast Processors LIVE</a></td>
                                    <td>The Blast Processors</td>
                                    <td>30 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51617446">Mega Man Battle Network 5</a></td>
                                    <td>Tterraj42</td>
                                    <td>4 hr 44 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51618731">Mega Man X5</a></td>
                                    <td>PixelKaye</td>
                                    <td>1 hr 25 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man 5 - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/51619715">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/51620391">2</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>1 hr 33 min</td>
                                    <td>Balloon animal hat</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51621292">Mega Man 2 race</a></td>
                                    <td>Noah McCarthy vs Steve Poissant</td>
                                    <td>2 hr 3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51623222">Mega Man X3</a></td>
                                    <td>SlappyMeats</td>
                                    <td>2 hr 3 min</td>
                                    <td>Hot Pepper Gauntlet</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/51624620">Mega Man 4</a></td>
                                    <td>ChubRockGeek</td>
                                    <td>1 hr 20 min</td>
                                    <td>Sandwich Run</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/49802099">HIGHLIGHT: PixelKaye Gets Truly Salty</a></td>
                                    <td>PixelKaye</td>
                                    <td>2 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/52119167">HIGHLIGHT: Hutch and his Balloon Hat</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>15 min</td>
                                    <td></td>
                                </tr>

                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->



                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
