<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url("/img/video/mm6-imagebar.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 65%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA MAN-ATHON 6 VIDEOS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">
                    
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr class="">
                                    <td>Game/Performance</td>
                                    <td>Player</td>
                                    <td>Video Length</td>
                                    <td>Details</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=VR9Y5HZdlBE">Mega Man-athon 6 promo</a></td>
                                    <td></td>
                                    <td>2 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/216838819?collection=DtOXe-cqCRXJTA">Introduction</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>2 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/216839652?collection=DtOXe-cqCRXJTA">Mega Man Powered Up</a></td>
                                    <td>Kazooaloo</td>
                                    <td>1 hr 14 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/216840788?collection=DtOXe-cqCRXJTA">Mega Man Network Transmission</a></td>
                                    <td>Tterraj42</td>
                                    <td>1 hr 25 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/216956883?collection=DtOXe-cqCRXJTA">Mega Man (Game Gear)</a></td>
                                    <td>The 8-Bit Duke</td>
                                    <td>1 hr 15 min</td>
                                    <td>Game Gear version</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217403675?collection=DtOXe-cqCRXJTA">Happy 30th Birthday Mega Man!</a></td>
                                    <td>Mega Man</td>
                                    <td>1 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217407240?collection=DtOXe-cqCRXJTA">NPCC Introduction</a></td>
                                    <td>NPCC</td>
                                    <td>8 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217408105?collection=DtOXe-cqCRXJTA">MC OHM-I LIVE</a></td>
                                    <td>MC OHM-I</td>
                                    <td>30 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217408928?collection=DtOXe-cqCRXJTA">Kadesh Flow LIVE</a></td>
                                    <td>Kadesh Flow</td>
                                    <td>29 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217410919?collection=DtOXe-cqCRXJTA">D&D Sluggers LIVE</a></td>
                                    <td>D&D Sluggers</td>
                                    <td>37 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217414240?collection=DtOXe-cqCRXJTA">Eye-Q LIVE</a></td>
                                    <td>Eye-Q</td>
                                    <td>39 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217416289?collection=DtOXe-cqCRXJTA">Amanda Lepre LIVE</a></td>
                                    <td>Amanda Lepre</td>
                                    <td>30 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217416701?collection=DtOXe-cqCRXJTA">Shubzilla & Bill Beats LIVE</a></td>
                                    <td>Shubzilla & Bill Beats</td>
                                    <td>37 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217417634?collection=DtOXe-cqCRXJTA">Sammus LIVE</a></td>
                                    <td>Sammus</td>
                                    <td>34 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217419790?collection=DtOXe-cqCRXJTA">Mega Man 9</a></td>
                                    <td>Epic Game Music</td>
                                    <td>1 hr 9 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217421392?collection=DtOXe-cqCRXJTA">Mega Man 2</a></td>
                                    <td>MegaMatt77</td>
                                    <td>1 hr</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217421902?collection=DtOXe-cqCRXJTA">Mega Man X3</a></td>
                                    <td>SlappyMeats</td>
                                    <td>2 hr 2 min</td>
                                    <td>Hot Pepper Gauntlet</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217422554?collection=DtOXe-cqCRXJTA">Mega Man Battle Network 3</a></td>
                                    <td>Tterraj42</td>
                                    <td>3 hr 58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217422954?collection=DtOXe-cqCRXJTA">Mega Man X2</a></td>
                                    <td>HCHecxz</td>
                                    <td>1 hr 37 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217424148?collection=DtOXe-cqCRXJTA">Mega Man X8</a></td>
                                    <td>LaceySan</td>
                                    <td>1 hr 36 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217424692?collection=DtOXe-cqCRXJTA">Mega Man 5</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>1 hr 27 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217425476?collection=DtOXe-cqCRXJTA">Mega Man 8</a></td>
                                    <td>PhoeneticHero</td>
                                    <td>1 hr 30 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217480246?collection=DtOXe-cqCRXJTA">Mega Man X4</a></td>
                                    <td>Jehm Faulking</td>
                                    <td>1 hr 10 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217480894?collection=DtOXe-cqCRXJTA">Mega Man Unlimited</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 16 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217481266?collection=DtOXe-cqCRXJTA">2 Mello LIVE</a></td>
                                    <td>2 Mello</td>
                                    <td>1 hr</td>
                                    <td>Wizard Party</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217481832?collection=DtOXe-cqCRXJTA">Steel Samurai LIVE</a></td>
                                    <td>Steel Samurai</td>
                                    <td>55 min</td>
                                    <td>Wizard Party</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217482609?collection=DtOXe-cqCRXJTA">Flabbercasters LIVE</a></td>
                                    <td>Flabbercasters</td>
                                    <td>36 min</td>
                                    <td>Wizard Party</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217482824?collection=DtOXe-cqCRXJTA">X-Hunters LIVE</a></td>
                                    <td>X-Hunters</td>
                                    <td>59 min</td>
                                    <td>Wizard Party</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217483129?collection=DtOXe-cqCRXJTA">Super Soul Bros. LIVE</a></td>
                                    <td>Super Soul Bros.</td>
                                    <td>1 hr2 min</td>
                                    <td>Wizard Party</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217488568?collection=DtOXe-cqCRXJTA">Rock Man</a></td>
                                    <td>Bryan Belcher & Jackson Parodi</td>
                                    <td>42 min</td> 
                                    <td>Jackson plays the stage music on accordian</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217489257?collection=DtOXe-cqCRXJTA">Mega Man X (Power Hour)</a></td>
                                    <td>HEET and Friends</td>
                                    <td>1 hr</td>
                                    <td>Each person gets 2 minutes to play at a time</td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man Board Game: Part <a href="https://www.twitch.tv/videos/217489770?collection=DtOXe-cqCRXJTA">1</a> - <a href="https://www.twitch.tv/videos/217489994?collection=DtOXe-cqCRXJTA">2</a> - <a href="https://www.twitch.tv/videos/217490166?collection=DtOXe-cqCRXJTA">3</a></td>
                                    <td>HEET and Friends</td>
                                    <td>4 hr 42 min</td>
                                    <td>House rules by CritsKrieger</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217886038?collection=DtOXe-cqCRXJTA">Mega Man IV (GameBoy)</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 27 min</td>
                                    <td>GameBoy version</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217887087?collection=DtOXe-cqCRXJTA">Mega Man V (GameBoy)</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 5 min</td>
                                    <td>GameBoy version</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217888094?collection=DtOXe-cqCRXJTA">Mega Man Xtreme</a></td>
                                    <td>The D-Pad</td>
                                    <td>2 hr 10 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217888938?collection=DtOXe-cqCRXJTA">Mega Man 10</a></td>
                                    <td>MegaMatt77</td>
                                    <td>1 hr 28 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217890240?collection=DtOXe-cqCRXJTA">The Misadventures of Tron Bonne</a></td>
                                    <td>BlueMetal</td>
                                    <td>45 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man Legends: Part <a href="https://www.twitch.tv/videos/217890504?collection=DtOXe-cqCRXJTA">1</a> - <a href="https://www.twitch.tv/videos/217890990?collection=DtOXe-cqCRXJTA">2</a></td>
                                    <td>BlueMetal</td>
                                    <td>55 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217892604?collection=DtOXe-cqCRXJTA">Street Fighter x Mega Man</a></td>
                                    <td>LaceySan</td>
                                    <td>31 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217893924?collection=DtOXe-cqCRXJTA">Juggling Show</a></td>
                                    <td>Juggleboy</td>
                                    <td>8 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217895207?collection=DtOXe-cqCRXJTA">FlexStyle LIVE</a></td>
                                    <td>FlexStyle</td>
                                    <td>23 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td>VGR LIVE: Part <a href="https://www.twitch.tv/videos/217895564?collection=DtOXe-cqCRXJTA">1</a> - <a href="https://www.twitch.tv/videos/217896300?collection=DtOXe-cqCRXJTA">2</a></td>
                                    <td>VGR</td>
                                    <td>28 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217897747?collection=DtOXe-cqCRXJTA">Ralfington LIVE</a></td>
                                    <td>Ralfington</td>
                                    <td>25 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217898194?collection=DtOXe-cqCRXJTA">Wishlyst LIVE</a></td>
                                    <td>Wishlyst</td>
                                    <td>27 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217898869?collection=DtOXe-cqCRXJTA">Jadeabella LIVE</a></td>
                                    <td>Jadeabella</td>
                                    <td>27 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217899559?collection=DtOXe-cqCRXJTA">James Landino LIVE</a></td>
                                    <td>James Landino</td>
                                    <td>31 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217900351?collection=DtOXe-cqCRXJTA">Ben Briggs LIVE</a></td>
                                    <td>Ben Briggs</td>
                                    <td>27 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217900978?collection=DtOXe-cqCRXJTA">Grimecraft LIVE</a></td>
                                    <td>Grimecraft</td>
                                    <td>49 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217901752?collection=DtOXe-cqCRXJTA">DJ Cutman LIVE</a></td>
                                    <td>DJ Cutman</td>
                                    <td>47 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217902822?collection=DtOXe-cqCRXJTA">Mega Man 8-Bit Deathmatch</a></td>
                                    <td>HEET + MAGFest attendees</td>
                                    <td>1 hr 7 min</td>
                                    <td>LAN tournament held at MAGFest</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217904584?collection=DtOXe-cqCRXJTA">Azure Striker Gunvolt</a></td>
                                    <td>MegaMatt77</td>
                                    <td>2 hr 7 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217905953?collection=DtOXe-cqCRXJTA">Mega Man DOS 1 & 3</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>48 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217908743?collection=DtOXe-cqCRXJTA">Mega Man: Dr. Wily's Final Attack</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>2 hr</td>
                                    <td>Developer TheSkipper1995 on the couch</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217909394?collection=DtOXe-cqCRXJTA">Mega Man Legends 2</a></td>
                                    <td>BlueMetal</td>
                                    <td>1 hr 30 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217910108?collection=DtOXe-cqCRXJTA">Mighty Gunvolt Burst</a></td>
                                    <td>Tterraj42</td>
                                    <td>27 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217910754?collection=DtOXe-cqCRXJTA">Rockman EXE</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 02 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217911349?collection=DtOXe-cqCRXJTA">Cannon Spike</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>45 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217911675?collection=DtOXe-cqCRXJTA">Shovel Knight: Specter of Torment</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>1 hr 25 min</td>
                                    <td>Mega Man X Code</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217912390?collection=DtOXe-cqCRXJTA">Mega Man 6</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 6 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/217913726?collection=DtOXe-cqCRXJTA">Mega Man-athon 6 Outro</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>4 min</td>
                                    <td></td>
                                </tr>

                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->



                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
