<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("/img/bryanpepper.jpg"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA MAN-ATHON 3 VIDEOS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">                   
                    <center><h3>Please note that due to network issues, a number of games are broken up into mulitple videos.</h3></center>
                    <br>
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr class="">
                                    <td><b>Video</b></td>
                                    <td><b>Player</b></td>
                                    <td><b>Video Length</b></td>
                                    <td><b>Details</b></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=2nbwjilLzlw">Mega Man-athon 3 Promo</a></td>
                                    <td></td>
                                    <td>2 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/playlist?list=PLjHkHamQfxKvs11DMwMnE87g-i3I2sh8U">Super Guitar Bros LIVE playlist</a></td>
                                    <td>Super Guitar Bros.</td>
                                    <td>50 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=1D2vlcaJTkQ">MegaFlare LIVE</a></td>
                                    <td>MegaFlare</td>
                                    <td>44 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/playlist?list=PLjHkHamQfxKvI31L2Z1goIecuWxvFVn9W">Sammus LIVE playlist</a></td>
                                    <td>Sammus</td>
                                    <td>40 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/playlist?list=PLjHkHamQfxKufAGXM9qqFQT3NTuvKFb9t">2 Mellow &amp My Paents Favortie Music LIVE</a></td>
                                    <td>2 Mello + My Parents Favorite Music</td>
                                    <td>49 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/playlist?list=PLjHkHamQfxKtbQrmj9q57U8ZPDtNgpc74">D&ampD Sluggers LIVE</a></td>
                                    <td>D&ampD Sluggers</td>
                                    <td>51 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41881176">Mega Man X4</a></td>
                                    <td>Jirard Khalil</td>
                                    <td>1 hr 38 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41881195">Mega Man</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41880788">Mega Man 2 (partial)</a></td>
                                    <td>Super Guitar Bros.</td>
                                    <td>52 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=VIYi2d_b3Q0">Mega Man 2 (partial) + Mega Man X (partial)</a></td>
                                    <td>Super Guitar Bros.</td>
                                    <td>36 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41873730">Mega Man 5</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>1 hr 27 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41867744">Mega Man: The Wily Wars</a></td>
                                    <td>Tony Ponce</td>
                                    <td>3 hr 35 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41867707">Mega Man DOS 1 &amp 3</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>49 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41867633">Mega Man &amp Bass</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 1 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41867132">Mega Man V</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 1 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41866903">Mega Man X2</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 26 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41866892">Mega Man 9</a></td>
                                    <td>MasterJoe116</td>
                                    <td>1 hr 39 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41861221">Mega Man X4</a></td>
                                    <td>JehmFaulking</td>
                                    <td>1 hr 20 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41861170">Mega Man Zero 2</a></td>
                                    <td>Phonetic Hero</td>
                                    <td>1 hr 44 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41861086">Mega Man Soccer</a></td>
                                    <td>Jairus and Bryan (HEET)</td>
                                    <td>24 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man ZX - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/41800803">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800571">2</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800577">3</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800701">4</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800694">5</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800837">6</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800540">7</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800722">8</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800743">9</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41800656">10</a></td>
                                    <td>HotSammySliz</td>
                                    <td>2 hr 46 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41743235">Mega Man X</a></td>
                                    <td>Bryan Belcher + Hot Pepper Gaming</td>
                                    <td>2 hr 36 min</td>
                                    <td>Bryan eats a habanero per death</td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man X - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/41635524">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41635035">2</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41635005">3</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41635058">4</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41635049">5</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41635008">6</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41634972">7</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41634962">8</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41634608">9</a></td>
                                    <td>Jirard Khalil</td>
                                    <td>1 hr 16 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41633450">Mega Man Battle Network 3 Blue</a></td>
                                    <td>TTerraj42</td>
                                    <td>3 hr 58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41633288">Mega Man Zero</a></td>
                                    <td>TTerraj42</td>
                                    <td>1 hr 52 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man Legends - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/41633130">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41633184">2</a></td>
                                    <td>Tabby</td>
                                    <td>2 hr 57 min</td>
                                    <td>Only highlights of the game</td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man X5 - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/41633149">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41630311">2</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 38 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man X6 - Part <a href="https://www.twitch.tv/halfemptyenergytank/v/41630358">1</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41630304">2</a>, 
                                    <a href="https://www.twitch.tv/halfemptyenergytank/v/41630417">3</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>3 hr 54 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41630308">Juggleboy juggles for Child's Play</a></td>
                                    <td>Juggleboy</td>
                                    <td>7 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41630151">Mega Man 7</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 25 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41629558">Mega Man Battle Network 6 Gregar</a></td>
                                    <td>Method1cal</td>
                                    <td>3 hr 26 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41629172">Mega Man 3</a></td>
                                    <td>Noah McCarthy</td>
                                    <td>56 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41629184">Mega Man Battle Network 2</a></td>
                                    <td>Tterraj42</td>
                                    <td>3 hr 12 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41629042">Mega Man Network Transmissions</a></td>
                                    <td>Kazooaloo</td>
                                    <td>2 hr 17 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41628927">Hutch and Jairus chit chat to fill time!</a></td>
                                    <td>Hutch and Jairus (HEET)</td>
                                    <td>26 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/halfemptyenergytank/v/41628793">Mega Man-athon 3 sign off</a></td>
                                    <td>HEET</td>
                                    <td>2 min</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->



                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
