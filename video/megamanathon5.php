<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/head.php'); ?>
    </head>
        
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/sidenav.php'); ?>

        <div class="page-wrap">
            <div class="top-half">

                <div class="image-bar">

                    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/header.php'); ?>

                    <style>
                        .image-bar{
                            background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url("/img/triforceQuartet.png"); 
                            background-repeat: no-repeat;
                            background-position: 100% 35%;
                            background-size: cover;
                        }
                    </style>
    

                    <div class="call-to-action fluid-container">
                        <h1>MEGA MAN-ATHON 5 VIDEOS</h1>                 
                    </div><!--end call-to-action-->
                
                </div><!--end image-bar-->

                <div class="main-content">
                    <div class="adjust-table container-fluid">
                    
                    <div class="row">
                        <div class="col-large-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-responsive">
                                <tr class="">
                                    <td>Game/Performance</td>
                                    <td>Player</td>
                                    <td>Video Length</td>
                                    <td>Details</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.youtube.com/watch?v=s3z62WoJysA">Mega Man-athon 5 promo</a></td>
                                    <td></td>
                                    <td>3 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114093889?collection=cA-X9Q0jqhQtTw">Mega Man 7</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>1 hr 16 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114095043?collection=cA-X9Q0jqhQtTw">Wily & Right no RockBoard: That's Paradise</a></td>
                                    <td>Game Dave</td>
                                    <td>1 hr 28 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114096339?collection=cA-X9Q0jqhQtTw">Mega Man</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>50 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114152923?collection=cA-X9Q0jqhQtTw">Chjolo LIVE</a></td>
                                    <td>Chjolo</td>
                                    <td>50 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114153768?collection=cA-X9Q0jqhQtTw">Pixel8tor LIVE</a></td>
                                    <td>Pixel8tor</td>
                                    <td>28 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114154712?collection=cA-X9Q0jqhQtTw">Ralfington LIVE</a></td>
                                    <td>Ralfington</td>
                                    <td>28 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114155378?collection=cA-X9Q0jqhQtTw">Grimecraft x Baroqueen LIVE</a></td>
                                    <td>Grimecraft x Baroqueen</td>
                                    <td>45 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114156386?collection=cA-X9Q0jqhQtTw">Ben Briggs LIVE</a></td>
                                    <td>Ben Briggs</td>
                                    <td>43 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114157373?collection=cA-X9Q0jqhQtTw">DjCUTMAN LIVE</a></td>
                                    <td>DjCUTMAN</td>
                                    <td>38 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114157891?collection=cA-X9Q0jqhQtTw">Mykah LIVE</a></td>
                                    <td>Mykah</td>
                                    <td>41 min</td>
                                    <td>GameChops Rave Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114159845?collection=cA-X9Q0jqhQtTw">Mega Man X4</a></td>
                                    <td>Jirard Khalil</td>
                                    <td>1 hr 50 min</td>
                                    <td>Zero run</td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man ZX - Part <a href="https://www.twitch.tv/videos/114160503?collection=cA-X9Q0jqhQtTw">1</a>, 
                                    <a href="https://www.twitch.tv/videos/114390218?collection=cA-X9Q0jqhQtTw">2</a></td>
                                    <td>HotSammySliz</td>
                                    <td>1 hr 20 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114391247?collection=cA-X9Q0jqhQtTw">Mega Man X8</a></td>
                                    <td>Kazooaloo</td>
                                    <td>1 hr 52 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114391915?collection=cA-X9Q0jqhQtTw">Mega Man Zero 3</a></td>
                                    <td>Tterraj42</td>
                                    <td>2hr 01 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114392775?collection=cA-X9Q0jqhQtTw">Mega Man Legends</a></td>
                                    <td>HCHecxz</td>
                                    <td>2 hr 21 min</td>
                                    <td>Highlights only</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114393929?collection=cA-X9Q0jqhQtTw">Mega Man Zero</a></td>
                                    <td>Jehm Faulking</td>
                                    <td>1 hr 22 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114394251?collection=cA-X9Q0jqhQtTw">Mega Man Unlimited</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>2 hr 26 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114395079?collection=cA-X9Q0jqhQtTw">Mega Man 4: BCAS</a></td>
                                    <td>PixelKaye</td>
                                    <td>58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114729980?collection=cA-X9Q0jqhQtTw">Mega Man DOS 1 &amp 3</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>56 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114731329?collection=cA-X9Q0jqhQtTw">Mega Man: Dr. Wily's Final Attack</a></td>
                                    <td>Bryan Belcher</td>
                                    <td>54 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114733092?collection=cA-X9Q0jqhQtTw">Doug Funnie LIVE</a></td>
                                    <td>Doug Funnie</td>
                                    <td>34 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114735948?collection=cA-X9Q0jqhQtTw">D&ampD Sluggers LIVE</a></td>
                                    <td>D&ampD Sluggers</td>
                                    <td>28 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114737147?collection=cA-X9Q0jqhQtTw">K-Murdock LIVE</a></td>
                                    <td>K-Murdock</td>
                                    <td>37 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114761535?collection=cA-X9Q0jqhQtTw">Kadesh Flow LIVE</a></td>
                                    <td>Kadesh Flow</td>
                                    <td>24 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114762809?collection=cA-X9Q0jqhQtTw">Creative Mind Frame/1-UP LIVE</a></td>
                                    <td>Creative Mind Frame/1-UP</td>
                                    <td>36 min</td> 
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114763327?collection=cA-X9Q0jqhQtTw">Mega Ran LIVE</a></td>
                                    <td>Mega Ran</td>
                                    <td>43 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114764704?collection=cA-X9Q0jqhQtTw">Shubzilla LIVE</a></td>
                                    <td>Shubzilla</td>
                                    <td>24 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114766676?collection=cA-X9Q0jqhQtTw">EyeQ LIVE</a></td>
                                    <td>EyeQ</td>
                                    <td>31 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114767304?collection=cA-X9Q0jqhQtTw">Sammus LIVE</a></td>
                                    <td>Sammus</td>
                                    <td>35 min</td>
                                    <td>NPCC Night</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114859651?collection=cA-X9Q0jqhQtTw">Mega Man 6</a></td>
                                    <td>Gaijin Goomba</td>
                                    <td>1 hr 24 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114861294?collection=cA-X9Q0jqhQtTw">Mega Man 2 Power Hour</a></td>
                                    <td>Content Productions</td>
                                    <td>1 hr 44 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114862227?collection=cA-X9Q0jqhQtTw">Mega Man 2: Atari Demake</a></td>
                                    <td>HEET</td>
                                    <td>1 hr 41 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114863365?collection=cA-X9Q0jqhQtTw">Mega Man 5 (GameBoy)</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 21 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114863827?collection=cA-X9Q0jqhQtTw">Mega Man Battle Network 6</a></td>
                                    <td>Tterraj42</td>
                                    <td>2 hr 58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/114863827?collection=cA-X9Q0jqhQtTw">Mega Man 3</a></td>
                                    <td>My Life in Gaming</td>
                                    <td>1 hr 45 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115129070?collection=cA-X9Q0jqhQtTw">Mega Man X2</a></td>
                                    <td>HCHecxz</td>
                                    <td>1 hr 47 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115129384?collection=cA-X9Q0jqhQtTw">Mega Man 9</a></td>
                                    <td>TheSkipper1995</td>
                                    <td>40 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115129741?collection=cA-X9Q0jqhQtTw">Azure Striker Gunvolt 2</a></td>
                                    <td>Matt Papa</td>
                                    <td>58 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115130331?collection=cA-X9Q0jqhQtTw">48 Robot Master 2 WayRace</a></td>
                                    <td>TheSkipper1995 &amp PixelKaye</td>
                                    <td>33 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115130664?collection=cA-X9Q0jqhQtTw">Triforce Quartet LIVE</a></td>
                                    <td>Triforce Quartet</td>
                                    <td>50 min</td>
                                    <td>HEET Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115130945?collection=cA-X9Q0jqhQtTw">Super Soul Bros. LIVE</a></td>
                                    <td>Super Soul Bros.</td>
                                    <td>15 min</td>
                                    <td>HEET Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115131285?collection=cA-X9Q0jqhQtTw">Professor Shyguy LIVE</a></td>
                                    <td>Professor Shyguy</td>
                                    <td>36 min</td>
                                    <td>HEET Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115131650?collection=cA-X9Q0jqhQtTw">Super Guitar Bros. LIVE</a></td>
                                    <td>Super Guitar Bros.</td>
                                    <td>48 min</td>
                                    <td>HEET Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115131838?collection=cA-X9Q0jqhQtTw">LucioPro LIVE</a></td>
                                    <td>LucioPro</td>
                                    <td>18 min</td>
                                    <td>HEET Showcase</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115132081?collection=cA-X9Q0jqhQtTw">Mega Man 8-bit Deathmatch</a></td>
                                    <td>HEET + MAGFest attendees</td>
                                    <td>1 hr 08 min</td>
                                    <td>LAN tournament held at MAGFest</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115132277?collection=cA-X9Q0jqhQtTw">Mega Man X</a></td>
                                    <td>Jirard Khalil</td>
                                    <td>1 hr 48 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115132533?collection=cA-X9Q0jqhQtTw">Mega Man Board Game</a></td>
                                    <td>HEET</td>
                                    <td>1 hr 42 min</td>
                                    <td>House rules by Critskrieger</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115132707?collection=cA-X9Q0jqhQtTw">Mega Man X3</a></td>
                                    <td>SlappyMeats</td>
                                    <td>2 hr 3 min</td>
                                    <td>Hot Pepper Gauntlet</td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115132964?collection=cA-X9Q0jqhQtTw">Mega Man Network Transmission</a></td>
                                    <td>Tterraj42</td>
                                    <td>1 hr 15 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td>Mega Man 8 - Part <a href="https://www.twitch.tv/videos/115133150?collection=cA-X9Q0jqhQtTw">1</a>, 
                                    <a href="https://www.twitch.tv/videos/115133308?collection=cA-X9Q0jqhQtTw">2</a></td>
                                    <td>PhoneticHero</td>
                                    <td>1 hr 17 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115133501?collection=cA-X9Q0jqhQtTw">Mega Man 4</a></td>
                                    <td>Hawkfoot718</td>
                                    <td>1 hr 02 min</td>
                                    <td></td>
                                </tr>
                                <tr class="">
                                    <td><a href="https://www.twitch.tv/videos/115133698?collection=cA-X9Q0jqhQtTw">Mega Man 5</a></td>
                                    <td>ItsTheHutch</td>
                                    <td>1 hr 33 min</td>
                                    <td></td>
                                </tr>

                            </table>
                        </div><!-- end col div -->
                    </div><!-- end row div -->
                </div><!-- end container-fluid div -->



                



            </div><!--end top-half-->
            
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php'); ?>
            
        </div><!--end page-wrap-->


        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/bottomscripts.php'); ?>
        
    </body>
</html>
